# Authentication

In order to interact with the SwitchPoint api, an authentication token is needed in the header of all requests. The token is distributed by Ifdef.

> To call an endpoint you need to add the following headers:

```http
GET /sp/v2/<endpoint> HTTP/1.1
Authorization: Bearer <token>
Host: https://api.ifdef.io
```

> Make sure to replace `<token>` with your API key.

To use the Switchpoint API the following headers must be added to every API call.

| Header        | Value           | Description                                            |
| ------------- | --------------- | ------------------------------------------------------ |
| Authorization | Bearer \<token> | Your private authentication token that identifies you. |

<aside class="notice">
You must replace <code>token</code> with your private API key.
</aside>

<!-- ## Management API

The management API is for partners to manage groups, tags, keys and swish connections.

### Login

> Login request

```http
POST /sp/v2/auth/login HTTP/1.1
Host: https://api.ifdef.io
Content-Type: application/x-www-form-urlencoded
Authorization: Basic <base64 encoded username:password>
```

> Successful response

```http
HTTP/1.1 200 OK
Content-Type: "application/json"

{
    "authentication_token": "<JWT token>"
}
```

> Error response

```http
HTTP/1.1 404 Not Found
Content-Type: "application/json"

{
    "error_code": "40400",
    "error_message": "Resource not found",
    "error_detail": [
        "User not found"
    ]
}
```

To get a JWT token for the management API the partner needs to login.
This endpoints uses basic authentication, and returns JWT token that expires in 2 hours.

`POST /sp/v2/auth/login` -->

<!-- ### Create Group

> Create a new group

```http
POST /sp/v2/groups HTTP/1.1
Host: https://api.ifdef.io
Content-Type: application/json
Authorization: Bearer <Token>

{
	"name": "documentation group"
}
```

> Successful response

```http
HTTP/1.1 201 Created
```

Create a new group for managing tags

`POST /sp/v2/groups`

### List groups

> List all groups

```http
GET /sp/v2/groups HTTP/1.1
Host: https://api.ifdef.io
Content-Type: application/json
Authorization: Bearer <Token>
```

> Successful response

```http
HTTP/1.1 200 OK
Content-Type: "application/json"

[
    {
        "id": "216ce76a7fd343e785aa7a0824001111",
        "name": "Example group 1"
    },
    {
        "id": "335306e49d914d9fa284932d9a1a1111",
        "name": "Example group 2"
    }
]
```

List all existing groups

`GET /sp/v2/groups`

### Add tag to group

> Add tag to group

```http
PUT /sp/v2/tags/:tagID HTTP/1.1
Host: https://api.ifdef.io
Content-Type: application/json
Authorization: Bearer <token>

{
	"group_id": {
		"value": "<Group ID>"
	}
}
```

> Successful response

```http
HTTP/1.1 200 Ok
```

Groups are used to group together all tags that share the same swish 123 number, for example all tags within a single store.

`PUT /sp/v2/tags/:tagID`

### Add swishpayment to group

> Add swish number to group

```http
POST /sp/v2/groups/:groupID/swish HTTP/1.1
Host: https://api.ifdef.io
Content-Type: application/json
Authorization: Bearer token

{
	"swish_123_number": "1234567891"
}
```

> Successful response

```http
HTTP/1.1 200 OK
```

Swish 123 number needs to be added to a group before a swish payment can be created for a tag within the group.

`POST /sp/v2/groups/:groupID/swish``

| Parameter | Description                                                  |
| --------- | ------------------------------------------------------------ |
| groupID   | The ID of the group the swish 123 number should be added too |

### Create Keys

> Create a new key

```http
POST /sp/v2/keys HTTP/1.1
Host: https://api.ifdef.io
Content-Type: application/json
Authorization: Bearer <token>

{
	"level": "PARTNER|GROUP|TAG",
	"id": "<id of the partner|group|tag>",
	"description": "documentation key"
}
```

> Successful response

```http
HTTP/1.1 201 Created
Content-Type: "application/json"
{
    "id": "239448c1a04342f9859cb844b8851111",
    "secret": "ed65665b63e54cc68fa8510715671111"
}
```

Keys are uses to access the session API

`POST /sp/v2/keys``

There are 3 types of keys

| Parameter | Description                                                                  |
| --------- | ---------------------------------------------------------------------------- |
| PARTNER   | Can be used to create a session for all tags owned by the partner            |
| GROUP     | Can be used to create a session for all tags belonging to the specific group |
| TAG       | Can only be used to create a session for a snigle tag                        |

<aside class="notice">
This is the only time that the secret will be displayed. The id secret combination must be stored.
</aside>

### List keys

> List all keys

```http
GET /sp/v2/keys HTTP/1.1
Host: https://api.ifdef.io
Content-Type: application/json
Authorization: Bearer <token>
```

> Successful response

```http
HTTP/1.1 200 OK
Content-Type: "application/json"
[
    {
        "id": "b862dc93dccb4fd68bc13936dba91111",
        "partner_id": "1D3yKDvSF58mhAOebIqm15N1111",
        "group_id": null,
        "level": "PARTNER",
        "valid": true,
        "description": "documentation key"
    }
]
```

Lists all keys managed py the partner

`GET /sp/v2/keys`

### Delete key

> Delete key

```http
DELETE /sp/v2/keys/:keyID HTTP/1.1
Host: https://api.ifdef.io
Content-Type: application/json
Authorization: Bearer <token>
```

> Successful response

```http
HTTP/1.1 204 No Content
```

A Key can be deleted when no longer needed or if it has been exposed

`DELETE /sp/v2/keys/:keyID` -->

# Session API

The session API handles all payments and interactions with the switchpoint.
To make a payment the host creates a new session towards a specific switchpoint and receives a sessionID. This enables the client to tap on the switchpoint and start the payment process. (See following picture for simplified overview).

![Swish flow](/images/Overview.png)

## Login

> Login request

```http
POST /sp/v2/auth/keys/login HTTP/1.1
Host: https://api.ifdef.io
Content-Type: application/x-www-form-urlencoded
Authorization: Basic <base64 encoded keyID:keySecret>
```

> Successful response

```http
HTTP/1.1 200 OK
Content-Type: "application/json"

{
    "authentication_token": "<JWT token>"
}
```

> Error response

```http
HTTP/1.1 404 Not Found
Content-Type: "application/json"

{
    "error_code": "40400",
    "error_message": "Resource not found",
    "error_detail": [
        "User not found"
    ]
}
```

To get a JWT token for the management API a store needs to login with a key/secret combination.
This endpoints uses basic authentication, and returns JWT token that expires in 1 day.

`POST /sp/v2/auth/keys/login`

## Create Session

> Create a session

```http
POST /sp/v2/host/:tagID HTTP/1.1
Host: https://api.ifdef.io
Content-Type: application/json
Authorization: Bearer <token>

{
	"flow": "SWISH",
	"data": {
		"payment_reference": "0123456789",
		"amount":            "100.00",
		"currency":          "SEK",
		"message":           "Kingston USB Flash Drive 8 GB"
	}
}
```

> Successful Response

```http
HTTP/1.1 201 Created
session-id: <sessionID>
```

The first step in a successful payment is to create a session

`POST /sp/v2/host/:tagID`

## Get Session


In order to keep the session alive and to receive the session result one Get Session request has to be active at any given time throughout the session. When a request returns the host has 5 seconds to send a new request or the session will be terminated.

The default timout for this request is 60 seconds. In that case the host is free to send a new request to extend the wait time.

> Get Session

```http
GET /sp/v2/host/:tagID/session HTTP/1.1
Host: https://api.ifdef.io
session-id: <sessionID>
Authorization: Bearer <token>
```

> Timeout

```http
HTTP/1.1 200 OK
session-id: <sessionID>

{
    "ns": "sp.flow.swish.done",
    "data": {
        "state": "INIT"
    }
}
```

> Session completed

```http
HTTP/1.1 200 OK
session-id: <sessionID>

{
    "ns": "sp.flow.swish.done",
    "data": {
        <Swish payment data>
    }
}
```

## Delete Session

> Delete session

```http
DELETE /sp/v2/host/:tagID/session HTTP/1.1
Host: https://api.ifdef.io
session-id: <sessionID>
Authorization: Bearer <token>
```

> Successful response

```http
HTTP/1.1 200 OK
```

Terminates the session immediately, Ongoing 'Get Session' request will return.

## Create refund

> Create refund

```http
POST /sp/v2/transfers/refund HTTP/1.1
Host: https://api.ifdef.io
Authorization: Bearer <token>

{ 
    "original_session_id": "<sessionID of a successful payment>",
    "amount": "<amount to refund>"
}
```

> Successful response

```http
HTTP/1.1 200 OK
session-id: <sessionID>

{
    "ns": "res.swish.refund.created",
    "data": {
        "id": "<Swish refund id>"
    }
}
```

Initiates a refund request. A successful request responds with a 'session-id' header that should be added to subsequent request as well as a response body showing the status of the refund.

### Request body

Parameter | Description
--------- | -----------
original_session_id | The session id from a successful payment
amount | The amount to refund (Should not exceed the original amount)

### Response headers

Parameter | Description
--------- | -----------
session-id | ID of the refund, should be used when checking the status of the refund

### Response body

Parameter | Description
--------- | -----------
ns | Refund status
data | Response object from swish

NS | Meaning
--------- | -----------
res.swish.refund.created | Refund has been successfully initiated with swish
res.swish.refund.created | Swish responded with error when creating refund

## Get refund

> Get refund

```http
GET /sp/v2/transfers/refund/session HTTP/1.1
Host: https://api.ifdef.io
session-id: <sessionID>
Authorization: Bearer <token>
```

> Successful response

```http
HTTP/1.1 200 OK
session-id: <sessionID>

{
    "ns": "res.swish.refund.complete",
    "data": {
        "id": "<Swish refund id>",
        "amount": "1.00",
        "status": "PAID",
        "currency": "SEK",
        "datePaid": "2021-09-28T23:09:08.623+0000",
        "payeeAlias": "<User phone number>",
        "payerAlias": "<merchant swish 123 number>",
        "dateCreated": "2021-09-28T23:09:00.536+0000"
    }
}
```

Checks the status of a refund request.

### Request headers

Parameter | Description
--------- | -----------
session-id | Session id of the refund taken from create refund request

### Response body

Parameter | Description
--------- | -----------
ns | Refund status
data | Response object from swish

NS | Meaning
--------- | -----------
res.swish.refund.ongoing | Refund is ongoing
res.swish.refund.complete | Refund successfully completed
res.swish.refund.error | Swish reported errors







 

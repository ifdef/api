# Actions

Each NFC tag has one action connected to it that determines what to do on a tap event.

The action is determined by the tag owner (host) and the first thing the client should do on a tap is to check the action.

### Fetch

Static data is stored for the tag and sent to the client upon a tap. The data on the tag can be updated at any time by the host.

> Example data stored on a tag

```json
{
  "ns": "ifdef.blip.fetch.example",
  "data": {
    "foo": "bar"
  }
}
```

The data stored on the tag is on the following format

| key  | type   | value                                      |
| ---- | ------ | ------------------------------------------ |
| ns   | string | The namespace identifing this type of data |
| data | json   | data object stored on the tag              |

### Channels

Data connection will be opened between the client and host where data can be exchanged between host and client.

The connection is initiated from the host and kept alive until the host closes it.

> Example message sent using CHANNEL connection

```json
{
  "ns": "ifdef.blip.channel.example",
  "data": {
    "foo": "bar"
  }
}
```

All messages sent should be on the following json format

| key  | type   | value                                         |
| ---- | ------ | --------------------------------------------- |
| ns   | string | The namespace identifing this type of message |
| data | json   | data object sent from host/client             |

# Client API

## Authentication

Endpoints for the Client API are unauthenticated. However a Session-ID is required to connect through an existing session.

## Channels

The client can communicate with the host when the host has initiated a channel. On a successful subscription a Session-ID is received and that Session-ID has to be used for all subsequent communication to that channel instance. A channel instance is closed when the host terminates the connection.

### Open channel

> Example of a GET request connecting to a channel

```http
GET /blip/v1/client/<tagid> HTTP/1.1
Content-Type: application/json
Authorization: Bearer <token>
Host: https://api.ifdef.io
```

```http
HTTP/1.1 200 OK
Session-ID: <sid>
```

`GET /blip/v1/client/<tagid>`

By sending a get request to the endpoint the client connects to a channel given that it has been created by a host.
The Session-ID can be read from the headers.

Upon receiving the Session-ID a new GET request should be sent using the Session-ID, that request will return with data from the other end or timeout after 60 seconds

It is the responsibility of the host to send a new GET request upon timeout or receiving data to maintain the channel.

### Send data

> Send data through the channel using POST request

```http
POST /blip/v1/client/<tagid> HTTP/1.1
Content-Type: application/json
Authorization: Bearer <token>
Host: https://api.ifdef.io

{
    "ns": "ifdef.blip.example.channel.post",
    "data": {
        "foo": "bar"
    }
}
```

> Successful response to the POST request

```http
HTTP/1.1 200 OK
Session-ID: <sid>
```

Data can only be sent after a the client has connected and received a Session-ID.

Make a normal http POST request with the Session-ID in the header.

`POST /blip/v1/client/<tagid>`

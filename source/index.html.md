---
title: API Reference

language_tabs: # must be one of https://git.io/vQNgJ
  - http

toc_footers:
  # - <a href='#'>Sign Up for a Developer Key</a>
  # - <a href='https://github.com/lord/slate'>Documentation Powered by Slate</a>

includes:
  - host_api
  # - client_api
  # - examples

search: true
---

# Introduction

Switchpoint is a mobile connectivity platform that enables mobile devices to asynchronously connect to host application via passive NFC tags. A mobile device is regarded as client and the host is most commonly any kind of back-end service.

![Switchpoint overview](/images/Switchpoint_principal_components.svg)

The SwitchPoint tags are designed and distributed by Ifdef for solution providers. Host and client applications can subscribe to SwitchPoint tags to create data channel between them when applicable.

The following documentation describes the API and flow of the SwitchPoint mobile connectivity platform.

<!-- ## Message format

> Example JSON message

```json
{
  "origin": "BLIP",
  "type" : "CLIENT_INFO",
  "version": 1,
  "data": {
    "msg": "connected"
  }
}
```

The Blip API is based on a JSON and every message sent uses the same base layout

| Header       | Value            | Description                                |
|--------------|------------------|--------------------------------------------|
|origin        | string           | The origin of the message.                               |
|type          | string           | The type of message given the origin.                    |
|version       | number           | The API version.                                         |
|data          | object           | The data being sent, layout depends on origin and type.  | -->
